STI-IT LDAP authentication
==========================

This entire repository contains (is...) a single ansible role.
This role sets up LDAP authentication at EPFL on Ubuntu and Debian machines for a given "group" as defined in https://groups.epfl.ch


Supported distributions
------------------------

The ansible role has been tested on:
  - Ubuntu 18.04
  - Ubuntu 20.04
  - Ubuntu 22.04
  - Debian 11

If you want to try/implement this on other (similar systems) look inside `tasks/`

Known bugs
----------

Don't put capital letters in group names


Role Variables
--------------

There is one main configurable variable to be set in `defaults/main.yml` that can be used: `sssd_simple_group_access`.
In the example it is "sti_cluster_team" which is a group defined in https://groups.epfl.ch (with LDAP and AD option) and must correspond to "sti_cluster_team_AppGrpU" on Active Directory.
This variable is used to update the `sssd.conf` in order to manage which LDAP group is allowed to authenticate on the machine.

There is a secondary variable `sssd_auth_cache` that can be set to True for laptops to enable caching of credentials to allow authentication when out of the EPFL network.



Using this role
----------------

To use this role you should clone this repository:

```bash
git clone https://gitlab.epfl.ch/sti-cluster/ansible_ldap_authentication.git
```

This will obviously create a folder called `ansible_ldap_authentication` which contains this ansible role.
You should then create an ansible playbook, let's say `playbook.yml` at the same level as the folder that calls the role:

```yml
---
- name: use ansible_ldap_authentication role playbook
  hosts: all
  user: root
  become: true
  roles:
    - role: ansible_ldap_authentication
```

This requires you to have set up hosts in `/etc/ansible/hosts` typically.
You can then run the playbook:


```bash
ansible-playbook playbook.yaml -k
```
(`-k` only needed if you haven't exported root's SSH key for passwordless login)


License
-------

BSD


Author Information
------------------

Emmanuel Jaep (emmanuel dot jaep at epfl dot ch).
Contibutions from Edward Andò (EPFL Center for Imaging)
